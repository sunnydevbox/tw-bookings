<?php

return [

    // Bookings Database Tables
    'tables' => [

        'bookings' => 'bookings',
        // 'booking_rates' => 'booking_rates',
        // 'booking_hierarchy' => 'booking_hierarchy',
        // 'booking_availability' => 'booking_availability',

    ],

    // Bookings Models
    'models' => [

        'booking' => \Sunnydevbox\TWBookings\Models\Booking::class,
        // 'booking_rate' => \Rinvex\Bookings\Models\BookingRate::class,
        // 'booking_availability' => \Rinvex\Bookings\Models\BookingAvailability::class,
    ],

];
