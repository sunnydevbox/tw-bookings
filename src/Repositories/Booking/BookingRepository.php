<?php
namespace Sunnydevbox\TWBookings\Repositories\Booking;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class BookingRepository extends TWBaseRepository
{
	public function model()
	{
		return config('tw-bookings.models.booking');
	}
}