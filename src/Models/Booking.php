<?php
namespace Sunnydevbox\TWBookings\Models;

use Sunnydevbox\TWCore\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends BaseModel
{
	use SoftDeletes;
	
	protected $fillable = [
		'assignable_type',
		'assignable_id',
		'bookable_type',
		'bookable_id',
	];
	
	public function getTable()
	{
		return config('tw-bookings.tables.bookings');
	}


	/**
     * Get all of the owning booking models.
     */
    public function bookable()
    {
        return $this->morphTo();
	}
	
	public function assignable()
    {
        return $this->morphTo();
    }
}