<?php
namespace Sunnydevbox\TWBookings\Traits;

use Illuminate\Support\Fluent;
use Illuminate\Contracts\Container\Container;

trait BookingTrait 
{
    /**
     * Get all of the owning commentable models.
     */
    public function bookings()
    {
        return $this->morphMany(config('tw-bookings.models.booking'), 'bookable');
    }
}