<?php
namespace Sunnydevbox\TWBookings;

use Sunnydevbox\TWCore\BaseServiceProvider;

class TWBookingsServiceProvider extends BaseServiceProvider
{

    public function registerProviders()
    {
        // $loader = \Illuminate\Foundation\AliasLoader::getInstance();

        // if (class_exists('\Sunnydevbox\TWCore\TWCoreServiceProvider')
        //     && !$this->app->resolved('\Sunnydevbox\TWCore\TWCoreServiceProvider')
        // ) {
        //     $this->app->register(\Sunnydevbox\TWCore\TWCoreServiceProvider::class);
        // }
    }

    public function registerCommands()
    {
    	if ($this->app->runningInConsole()) {

            $this->commands([
                \Sunnydevbox\TWBookings\Console\Commands\PublishConfigCommand::class,
                \Sunnydevbox\TWBookings\Console\Commands\PublishMigrationsCommand::class,
            ]);
        }
    }

    public function registerPublishers()
    {
         $this->publishes([
            __DIR__ . '/../config/config.php' => config_path('tw-bookings.php'),
        ], 'config');


        $this->mergeConfigFrom(
            __DIR__ . '/../config/config.php', 'tw-bookings'
        );       
    }
}