<?php
namespace Sunnydevbox\TWBookings\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class BookingController extends APIBaseController
{
    public function __construct(
		\Sunnydevbox\TWBookings\Repositories\Booking\BookingRepository $repository, 
		\Sunnydevbox\TWBookings\Validators\BookingValidator $validator,
		\Sunnydevbox\TWBookings\Transformers\BookingTransformer $transformer
	) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
    }
}