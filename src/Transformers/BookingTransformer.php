<?php
namespace Sunnydevbox\TWBookings\Transformers;

use Dingo\Api\Http\Request;
use League\Fractal\TransformerAbstract;
use Sunnydevbox\TWEvents\Transformers\EventTransformer;

class BookingTransformer extends TransformerAbstract
{
    public $availbleIncludes = [
        'assignable',
        'bookable',
    ];

    public function transform($obj)
    {
        //dd($obj);
        $data = [
            'id'        	=> (int) $obj->id,
            'status' 	   	=> (boolean) $obj->status,
        ];

        return $data;
    }

    public function includeBookable($model) 
    {
        return $this->item($model->bookable, new BookingBookableTransformer);
    }

    public function includeAssignable($model)
    {   
        return $this->item($model->assignable, new BookingOwnerTransformer);
    }
}