<?php
namespace Sunnydevbox\TWBookings\Transformers;

use Dingo\Api\Http\Request;
use League\Fractal\TransformerAbstract;
use Sunnydevbox\TWEvents\Transformers\EventTransformer;

class BookingBookableTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'assignable'
    ];

    public function transform($obj)
    {
        return $obj ->toArray();
    }

    public function includeAssignable($obj)
    {
        return $this->item($obj->assignable, new BookingOwnerTransformer);
    }
}