<?php
namespace Sunnydevbox\TWBookings\Transformers;

use Dingo\Api\Http\Request;
use League\Fractal\TransformerAbstract;
use Sunnydevbox\TWEvents\Transformers\EventTransformer;

class BookingOwnerTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        return $obj ->toArray();
    }
}