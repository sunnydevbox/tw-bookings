<?php

namespace Sunnydevbox\TWBookings\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Support\Facades\Artisan;

class PublishMigrationsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twbookings:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate Rinvex Bookings Tables.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Running Bookings Migration');
        $this->call('migrate', ['--step' => true, '--path' => 'vendor/sunnydevbox/tw-bookings/database/migrations']);
    }

    public function fire()
    {
        echo 'fire';
    }
}
