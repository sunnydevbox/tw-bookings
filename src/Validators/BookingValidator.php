<?php
namespace Sunnydevbox\TWBookings\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class BookingValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'bookable_type'     => 'required',
            'bookable_id'       => 'required',
            'assignable_type'     => 'required',
            'assignable_id'       => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'bookable_type'     => 'required',
            'bookable_id'       => 'required',
            'assignable_type'     => 'required',
            'assignable_id'       => 'required',
        ]
   ];

}   